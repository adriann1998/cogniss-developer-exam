const jsonfile = require("jsonfile");
const randomstring = require("randomstring");

const inputFile = "input2.json";
const outputFile = "input3.json"

let output = {};
console.log(`Reading data from ${inputFile}`);
jsonfile.readFile(inputFile, function (err, body) {
  console.log(`Generating emails`);
  let emails = body.names
    .map((s) => s.toLowerCase().split("").reverse().join("")) // reverse
    .map((s) => `${s}${randomstring.generate(5)}@gmail.com`); // add random string and @gmail.com
  output.emails = emails;
  console.log(`Writing emails to ${outputFile}`)
  jsonfile.writeFile(outputFile, output, { spaces: 2 }, function (err) {
    err ? console.log(err) : console.log("Writing succeded");
  });
});
